﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Online_Book_Store
{
    public partial class LoginScreen : Form
    {
        //For move the form.       
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        UserDashboard ud = new UserDashboard();
        AdminDashboard ad = new AdminDashboard();
        SqltoObj so = new SqltoObj();        
        int userindex;
        public static string userName;


        public LoginScreen()
        {
            InitializeComponent();
            
           
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblSgn_MouseHover(object sender, EventArgs e)
        {
            lblSgn.ForeColor = Color.Red;
        }

        private void lblSgn_MouseLeave(object sender, EventArgs e)
        {
            lblSgn.ForeColor =Color.FromArgb (0, 71, 160);
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, 0xA1, 0x2, 0);
            }
        }

        private void btnLgn_Click(object sender, EventArgs e)
        {
            so.pullData();
            userName = txtUsr.Text;
            

            for (int i=0;i< so.getList().Count;i++)
            {
                if(txtUsr.Text== so.getList()[i].CustomerUsername&&txtPwd.Text== so.getList()[i].CustomerPassword&& so.getList()[i].IsAdmin_==false)
                {
                    
                    ud.Show();
                    this.Hide();
                    userindex = i;
                    ud.userindex = this.userindex;
                    break;
                    
                    
                }

                else if(txtUsr.Text == so.getList()[i].CustomerUsername && txtPwd.Text == so.getList()[i].CustomerPassword && so.getList()[i].IsAdmin_ == true)
                {
                    userName = "Admin";
                    ad.ShowDialog();
                    this.Hide();
                    break;
                }

                
            }
            Log.Logging(userName, this.Name, "Logged", DateTime.Now.ToString());

        }

        
        private void lblSgn_Click(object sender, EventArgs e)
        {
            Log.Logging(userName, this.Name, "Clicked to register for the New User", DateTime.Now.ToString());
            RegisterScreen rs = new RegisterScreen();
            rs.chkAdmin.Visible = false;
            this.Hide();

            while(rs.IsDisposed==false)
            {
                rs.ShowDialog();
            }

            this.Show();
            
        }

        private void LoginScreen_Load(object sender, EventArgs e)
        {

        }
    }
}
