﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class AddProduct : Form
    {
        Book book;
        Magazine magazine;
        MusicCD music;
        

        public AddProduct()
        {
            InitializeComponent();
        }
       

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        
    
        private void btnOk_Click_1(object sender, EventArgs e)
        {
            if (cmbType.SelectedIndex==0)
            {
               
                lblISBN.Text =   "ISBN          :";
                lblAuthor.Text = "Author      :";
                txtPublish.Enabled = true;
                txtPage.Enabled = true;

            }

            else if (cmbType.SelectedIndex == 1)
            {
                
                lblISBN.Text =   "Issue         :";
                lblAuthor.Text = "Type         :";
                txtPublish.Enabled = false;
                txtPage.Enabled = false;
                
            }


            else if (cmbType.SelectedIndex == 2)
            {
               
                lblISBN.Text =   "Singer      :";
                lblAuthor.Text = "Type         :";
                txtPublish.Enabled = false;
                txtPage.Enabled = false;
            }

            else
            {
                MessageBox.Show("Please Select Type!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            Log.Logging(LoginScreen.userName, this.Name, "Clicked for the type of the Product ", DateTime.Now.ToString());

        }

      
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (cmbType.SelectedIndex == 0)
            {
                book = new Book();
                book.addToDB(txtName, txtPrice, txtIsbn, txtAuthor, txtPublish, txtPage, txtPath);
              
            }

            else if (cmbType.SelectedIndex == 1)
            {
                magazine = new Magazine();
                magazine.addToDB(txtName, txtPrice, txtIsbn, txtAuthor, txtPath);

            }


            else if (cmbType.SelectedIndex == 2)
            {
                music = new MusicCD();
                music.addToDB(txtName, txtPrice, txtIsbn, txtAuthor, txtPath);
                
            }

            else
            {
                MessageBox.Show("Please Select Type!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add the Product ", DateTime.Now.ToString());
        }
    }
}
