﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class Cart : Form
    {
        ShoppingCart crt = ShoppingCart.GetInstance();

        public Cart()
        {
            InitializeComponent();           

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
            
        }

        private void Cart_Load(object sender, EventArgs e)
        {
            dgvProducts.DataSource = crt.printProducts();
            lblAmount.Text = crt.PaymentAmount + " TL";
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            foreach (DataGridViewRow row in dgvProducts.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value))
                {
                    
                    for(int i=0;i<crt.printProducts().Count;i++)
                    {
                      
                            if (row.Cells[1].Value.ToString() == crt.printProducts()[i].Product)
                            {

                                crt.PaymentAmount -=  crt.printProducts()[i].Quantity*crt.printProducts()[i].Price;
                                crt.removeProduct(crt.printProducts()[i]);
                                lblAmount.Text = crt.PaymentAmount + " TL";

                            }
                        
                        
                    }
                                        
                }
            }
            dgvProducts.DataSource = null;
            dgvProducts.DataSource = crt.printProducts();
            dgvProducts.Refresh();

            MessageBox.Show("Items removed from cart!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to remove the Products from the Shopping Cart", DateTime.Now.ToString());

        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Payment is successfull!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to pay for the Products", DateTime.Now.ToString());
        }
    }
}
