﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Online_Book_Store
{
    public partial class RegisterScreen : Form
    {
        SqlCnnct con = new SqlCnnct();
        Customer cstm = new Customer();
        //For move the form.       
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public RegisterScreen()
        {
            InitializeComponent();
           
        }       
        

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, 0xA1, 0x2, 0);
            }
        }

       

        private void btnRegister_Click(object sender, EventArgs e)
        {
            
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }


        private void btnExit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnRegister_Click_1(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Registration confirmed", DateTime.Now.ToString());
            cstm.AddtoDB(txtName, txtEmail, txtAddress, txtUsername, txtPassword, chkAdmin);
            
        }
    }
}
