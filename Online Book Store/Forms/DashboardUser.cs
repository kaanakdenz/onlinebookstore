﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Online_Book_Store
{
    public partial class UserDashboard : Form
    {
        int panelWidth;
        bool isCollapsed;
        SqltoObj so = new SqltoObj();
        public int userindex;
        int a = 0;

        public UserDashboard()
        {
            InitializeComponent();                        
            timer2.Start();
            panelWidth = panelLeft.Width;
            isCollapsed = false;
            so.pullData();
            so.pullBookData();
            

        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {

            System.Environment.Exit(1);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                panelLeft.Width = panelLeft.Width + 10;
                if (panelLeft.Width >= panelWidth)
                {

                    timer1.Stop();
                    isCollapsed = false;
                    this.Refresh();
                }

            }

            else
            {
                panelLeft.Width = panelLeft.Width - 10;
                if (panelLeft.Width <= 80)
                {
                    timer1.Stop();
                    isCollapsed = true;
                    this.Refresh();
                }

            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            if (isCollapsed)
            {
                lbName.Show();
                pbIco.Show();
                pbIco2.Hide();
            }
            else
            {
                lbName.Hide();
                pbIco.Hide();
                pbIco2.Show();

            }
        }

        private void moveSidePanel(Control btn)
        {

            panelSide.Top = btn.Top;
            panelSide.Height = btn.Height;

        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnBook);
            
            UC_UserBook ucBook = new UC_UserBook();
            AddControlsToPanel(ucBook);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to get in Book Page", DateTime.Now.ToString());
        }

        private void btnMagazine_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnMagazine);
            UC_UserMagazine ucMag = new UC_UserMagazine();
            AddControlsToPanel(ucMag);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to get in Magazine Page", DateTime.Now.ToString());
        }

        private void btnMusic_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnMusic);
            UC_UserMusic ucMus = new UC_UserMusic();
            AddControlsToPanel(ucMus);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to get in Book Page", DateTime.Now.ToString());
        }

        private void btnShoppingCart_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnShoppingCart);
            Cart crt = new Cart();
            crt.Show();
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to get in Shopping Cart Page", DateTime.Now.ToString());
        }
        
        private void timer2_Tick(object sender, EventArgs e)
        {
            a = 0;
                if (a == 0)
            {
                lblUser.Text = so.getList()[userindex].CustomerUsername;
            
                a++;
            }
            DateTime dt = DateTime.Now;
            lblTime.Text = dt.ToLongTimeString();
        }


        private void AddControlsToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panelControls.Controls.Clear();
            panelControls.Controls.Add(c);

        }

        private void UserDashboard_Load(object sender, EventArgs e)
        {
            this.btnBook_Click(sender, e);
        }
    }

}