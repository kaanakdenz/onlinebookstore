﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class AdminDashboard : Form
    {
        int panelWidth;
        bool isCollapsed;

        public AdminDashboard()
        {
            InitializeComponent();
            panelWidth = panelLeft.Width;
            isCollapsed = false;
        }

        private void AddControlsToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panelControls.Controls.Clear();
            panelControls.Controls.Add(c);

        }

        private void btnProducts_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnProducts);
            UC_AdminProduct ucAP = new UC_AdminProduct();
            AddControlsToPanel(ucAP);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to edit for the Products", DateTime.Now.ToString());
        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            moveSidePanel(btnCustomers);
            UC_AdminCustomer ucAC = new UC_AdminCustomer();
            AddControlsToPanel(ucAC);
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to edit for the Customers", DateTime.Now.ToString());
        }

        private void moveSidePanel(Control btn)
        {
            panelSide.Top = btn.Top;
            panelSide.Height = btn.Height;

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        

        private void btnMenu_Click(object sender, EventArgs e)
        {
            timer1.Start();
            if (isCollapsed)
            {
                lbName.Show();
                pbIco.Show();
                pbIco2.Hide();
            }
            else
            {
                lbName.Hide();
                pbIco.Hide();
                pbIco2.Show();

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                panelLeft.Width = panelLeft.Width + 10;
                if (panelLeft.Width >= panelWidth)
                {

                    timer1.Stop();
                    isCollapsed = false;
                    this.Refresh();
                }

            }

            else
            {
                panelLeft.Width = panelLeft.Width - 10;
                if (panelLeft.Width <= 80)
                {
                    timer1.Stop();
                    isCollapsed = true;
                    this.Refresh();
                }

            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            lblTime.Text = dt.ToLongTimeString();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void AdminDashboard_Load(object sender, EventArgs e)
        {
            this.btnCustomers_Click(sender, e);
        }
    }
    }
