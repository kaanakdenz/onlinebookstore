﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Online_Book_Store
{
    class SqltoObj
    {
        private SqlCnnct con = new SqlCnnct();
        private DataTable userTable=new DataTable();
        private DataTable bookTable = new DataTable();
        private DataTable magazineTable=new DataTable();
        private DataTable musicTable = new DataTable();
        private DataTable products = new DataTable();
        private MusicCD music;
        private Magazine mgzn;
        private Customer cstm;
        private Book book;
        private List<Customer> customers = new List<Customer>();
        private List<Book> books = new List<Book>();
        private List<Magazine> magazines= new List<Magazine>();
        private List<MusicCD> musics = new List<MusicCD>();
       

     

        public void pullData()

        {
            userTable.Clear();
            con.Connection();
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM USERTABLE",con.Cnnct);
            da.Fill(userTable);
            con.Close();

            for (int i = 0; i < userTable.Rows.Count; i++)
            {
                cstm= new Customer();
                cstm.CustomerName = (userTable.Rows[i]["NAME"]).ToString();
                cstm.CustomerAdress = userTable.Rows[i]["Address"].ToString();
                cstm.CustomerEmail = userTable.Rows[i]["EMAIL"].ToString();
                cstm.CustomerUsername = userTable.Rows[i]["USERNAME"].ToString();
                cstm.CustomerPassword = userTable.Rows[i]["PASSWORD"].ToString();
                cstm.IsAdmin_=Convert.ToBoolean(userTable.Rows[i]["ISADMIN"].ToString());
                customers.Add(cstm);
            }
        }

        public void pullBookData()
        {
            bookTable.Clear();
            con.Connection();
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("SELECT P.NAME,P.PRICE,B.ISBN,B.AUTHOR,B.PUBLISHER,B.PAGE,B.COVERPAGE FROM PRODUCTTABLE P,BOOKTABLE B WHERE B.ID=P.ID", con.Cnnct);
            da.Fill(bookTable);            
            con.Close();


            for (int i = 0; i < bookTable.Rows.Count; i++)
            {
                book = new Book();
                book.ProductName_ = (bookTable.Rows[i]["NAME"]).ToString();
                book.ProductPrice_ = Convert.ToInt32( (bookTable.Rows[i]["PRICE"]).ToString());
                book.BookISBN_= Convert.ToInt32((bookTable.Rows[i]["ISBN"]).ToString());
                book.BookAuthor_ = (bookTable.Rows[i]["AUTHOR"]).ToString();
                book.BookPublisher_ = (bookTable.Rows[i]["PUBLISHER"]).ToString();
                book.Page = Convert.ToInt32((bookTable.Rows[i]["PAGE"]).ToString());
                book.CoverPage= (bookTable.Rows[i]["COVERPAGE"]).ToString();               
                books.Add(book);
            }
                       

        }

        public void pullMagazineData()
        {
            magazineTable.Clear();
            con.Connection();
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("SELECT P.NAME,P.PRICE,M.ISSUE,M.TYPE,M.COVERPAGE FROM PRODUCTTABLE P,MAGAZINETABLE M WHERE P.ID=M.ID", con.Cnnct);
            da.Fill(magazineTable);
            con.Close();


            for (int i = 0; i < magazineTable.Rows.Count; i++)
            {
                mgzn = new Magazine();
                mgzn.ProductName_= (magazineTable.Rows[i]["NAME"]).ToString();
                mgzn.ProductPrice_= Convert.ToInt32((magazineTable.Rows[i]["PRICE"]).ToString());
                mgzn.MagazineIssue= Convert.ToInt32((magazineTable.Rows[i]["ISSUE"]).ToString());
                mgzn.MagazineType= (magazineTable.Rows[i]["TYPE"]).ToString();                
                mgzn.Coverpage = (magazineTable.Rows[i]["COVERPAGE"]).ToString();
                magazines.Add(mgzn);
            }


        }

        public void pullMusicData()
        {
            musicTable.Clear();
            con.Connection();
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("SELECT P.NAME,P.PRICE,S.SINGER,S.TYPE,S.COVERPAGE FROM PRODUCTTABLE P,MUSICTABLE S WHERE P.ID=S.ID", con.Cnnct);
            da.Fill(musicTable);
            con.Close();


            for (int i = 0; i < musicTable.Rows.Count; i++)
            {
                music = new MusicCD();
                music.ProductName_= (musicTable.Rows[i]["NAME"]).ToString();
                music.ProductPrice_= Convert.ToInt32((musicTable.Rows[i]["PRICE"]).ToString()); 
                music.MusicCDSinger_= (musicTable.Rows[i]["SINGER"]).ToString();
                music.MusicCDType_= (musicTable.Rows[i]["TYPE"]).ToString();              
                music.Coverpage = (musicTable.Rows[i]["COVERPAGE"]).ToString();
                musics.Add(music);
            }

        }

        public void mergeTable()
        {

            pullBookData();
            pullMagazineData();
            pullMusicData();

            products.Merge(bookTable);
            products.Merge(magazineTable);
            products.Merge(musicTable);


        }

        public DataTable getProductTable()
        {

            return products;
        }
    
        public List<Magazine> GetMagazines()
        {

            return magazines;
        }

        public List<Customer> getList()
        {

            return customers;
        }


        public List<Book> getBookList()
        {

            return books;
        }
        
        public List<MusicCD> getMusicList()
        {

            return musics;
        }

        public DataTable getmusicSrc()
        {

            return musicTable;

        }
        public DataTable getDTSource()
        {

            return userTable;
        }

        public DataTable getBookSource()
        {

            return bookTable;
        }


        public DataTable getMgznSource()
        {

            return magazineTable;

        }

    }

   }

