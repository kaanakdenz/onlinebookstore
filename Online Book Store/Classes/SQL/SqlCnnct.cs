﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Online_Book_Store
{
    class SqlCnnct
    {
        private SqlConnection cnnct;

        public SqlConnection Cnnct { get => cnnct; set => cnnct = value; }

        public void Connection()
        {
            cnnct= new SqlConnection("Data Source=DESKTOP-7JN2J2L;Initial Catalog=OnlineBookStore;Integrated Security=True");
        }

        public void Open()
        {
            cnnct.Open();
        }

        public void Close()
        {
            cnnct.Close();
        }
    }
}
