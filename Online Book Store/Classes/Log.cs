﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Online_Book_Store
{
    class Log
    {
        public static void Logging(string User, string Location, string Event, string DateTime)
        {
            string LogData = (String.Format("{0,-20} | {1,-25} | {2,-65} | {3,-30}", "User:" + User, "Location:" + Location, "Event:" + Event, "Time:" + DateTime));
            StreamWriter sw = new StreamWriter("log.txt", true);
            sw.WriteLine(LogData);
            sw.Close();
        }
    }
}
