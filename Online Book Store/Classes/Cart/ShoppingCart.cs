﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Online_Book_Store
{
    class ShoppingCart
    {
        private int customerID;
        private int paymentAmount;
        private string paymentType;
        private List<ItemToPurchase> itemlist=new List<ItemToPurchase>();

        public int PaymentAmount { get => paymentAmount; set => paymentAmount = value; }
        public string PaymentType { get => paymentType; set => paymentType = value; }
        public int CustomerID { get => customerID; set => customerID = value; }

        private static ShoppingCart _shoppingCart;

        private ShoppingCart() {  }

        public static ShoppingCart GetInstance()
        {

            if (_shoppingCart == null)
                _shoppingCart = new ShoppingCart();
            return _shoppingCart;
        }
    

    public void addProduct(ItemToPurchase i)
        {
            itemlist.Add(i);
        }

        public void removeProduct(ItemToPurchase i)
        {
            itemlist.Remove(i);
        }

        public List<ItemToPurchase> printProducts()
        {

            return itemlist;
        }

        //placeorder
        //cancelorder
        //sendinvoicedbyemail

    }
}
