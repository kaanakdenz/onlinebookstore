﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Online_Book_Store
{
    class Customer
    {
          
        private string Name_;
        private string Adress_;
        private string Email_;
        private string Username_;
        private string Password_;
        private bool isAdmin_;
     
      
        public string CustomerName
        {
            get { return Name_; }
            set { Name_ = value; }
        }
        public string CustomerAdress
        {
            get { return Adress_; }
            set { Adress_ = value; }
        }
        public string CustomerEmail
        {
            get { return Email_; }
            set { Email_ = value; }
        }
        public string CustomerUsername
        {
            get { return Username_; }
            set { Username_ = value; }
        }
        public string CustomerPassword
        {
            get { return Password_; }
            set { Password_ = value; }
        }

        public bool IsAdmin_
        { get => isAdmin_; set => isAdmin_ = value; }

        public Customer(string Name, string Address, string Email, string Username, string Password)
        {
            this.CustomerName = Name;
            this.CustomerAdress = Address;
            this.CustomerEmail = Email;
            this.CustomerUsername = Username;
            this.CustomerPassword = Password;
        }
            

        public Customer() { }     
        
        public void AddtoDB(TextBox txtName, TextBox txtAddress, TextBox txtEmail, TextBox txtUsername, TextBox txtPassword,CheckBox isAdmin)
        {
            try
            {
                SqlCnnct con = new SqlCnnct();
                con.Connection();
                con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO USERTABLE(NAME,ADDRESS,EMAIL,USERNAME,PASSWORD,ISADMIN) VALUES (@Name,@Address,@Email,@Username,@Password,@Isadmin)", con.Cnnct);
                if (txtName.Text == "" || txtAddress.Text == "" || txtEmail.Text == "" || txtUsername.Text == "" || txtPassword.Text == "")
                {
                    MessageBox.Show("Please fill the blanks!", "WARNING!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Username", txtUsername.Text);
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Text);
                    cmd.Parameters.AddWithValue("@Isadmin", isAdmin.Checked);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Registration is succeed!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }

            catch
            {
                MessageBox.Show("Registration is not succeed!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            


        }

    }

      
    }

