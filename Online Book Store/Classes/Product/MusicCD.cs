﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    class MusicCD: Product
    {
        public enum MusicType { Jazz, Pop, Rock, Blues, TSM };
        
        private string MusicCDSinger;
        private string MusicCDType;
        private string MusicCDIssue;
        private string coverpage;

        public string MusicCDSinger_
        {
            get { return MusicCDSinger; }
            set { MusicCDSinger = value; }
        }
        public string MusicCDType_
        {
            get { return MusicCDType; }
            set { MusicCDType = value; }
        }
        public string MusicCDIssue_
        {
            get { return MusicCDIssue; }
            set { MusicCDIssue = value; }
        }

        public string Coverpage { get => coverpage; set => coverpage = value; }

        public void addToDB(TextBox txtName, TextBox txtPrice, TextBox txtIssue, TextBox txtType, TextBox txtCoverpage)
        {

            try
            {
                SqlCnnct con = new SqlCnnct();
                con.Connection();
                con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO PRODUCTTABLE(NAME,PRICE) VALUES (@Name,@Price)", con.Cnnct);
                SqlCommand cmd2 = new SqlCommand("INSERT INTO MUSICTABLE(SINGER,TYPE,COVERPAGE) VALUES (@Singer,@Type,@Coverpage)", con.Cnnct);

                if (txtName.Text == "" || txtPrice.Text == "" || txtIssue.Text == "" || txtType.Text == "" || txtCoverpage.Text == "")
                {
                    MessageBox.Show("Please fill the blanks!", "WARNING!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {

                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Price", txtPrice.Text);
                    cmd.ExecuteNonQuery();
                    //SqlCommand command = new SqlCommand("SELECT ID FROM PRODUCTTABLE WHERE NAME ='" + txtName.Text + "'", con.Cnnct);
                    //SqlDataReader reader = command.ExecuteReader();
                    //reader.Read();
                    //int p=reader.GetInt32(0);
                    //cmd2.Parameters.AddWithValue("@Id", Convert.ToInt32(p));
                    cmd2.Parameters.AddWithValue("@Singer", txtIssue.Text);
                    cmd2.Parameters.AddWithValue("@Type", txtType.Text);
                    cmd2.Parameters.AddWithValue("@Coverpage", txtCoverpage.Text);
                    cmd2.ExecuteNonQuery();

                    con.Close();
                    MessageBox.Show("Registration is succeed!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }

            catch
            {
                MessageBox.Show("Registration is not succeed!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }


    

}
}
