﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Online_Book_Store
{
    public abstract class Product
    {
        private string ProductName;
        private float ProductPrice;
        public int ProductID;
        public void printProperties() { }

        public string ProductName_
        {
            get { return ProductName; }
            set { ProductName = value; }
        }
        public float ProductPrice_
        {
            get { return ProductPrice; }
            set { ProductPrice = value; }
        }
        public int ProductID_
        {
            get { return ProductID; }
            set { ProductID = value; }
        }

    }
}
