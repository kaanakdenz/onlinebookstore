﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Online_Book_Store
{
    class Book: Product
    {
        private int bookISBN;
        private string bookAuthor;
        private string bookPublisher;
        private string coverPage;
        private int page;
        [NotMapped]
        
       

        public int BookISBN_
        {
            get { return bookISBN; }
            set { bookISBN = value; }
        }
        public string BookAuthor_
        {
            get { return bookAuthor; }
            set { bookAuthor = value; }
        }
        public string BookPublisher_
        {
            get { return bookPublisher; }
            set { bookPublisher = value; }
        }

        public Bitmap Pic
        {
            get
            {
                if(!string.IsNullOrEmpty(coverPage))
                {
                    if(File.Exists(coverPage))
                    {
                        return new Bitmap(coverPage); ;
                    }                    

                }
                return null;
            }


        }

        public string CoverPage { get => coverPage; set => coverPage = value; }
        public int Page { get => page; set => page = value; }

        public void addToDB(TextBox txtName, TextBox txtPrice, TextBox txtISBN, TextBox txtAuthor, TextBox txtPage, TextBox txtPublisher, TextBox txtCoverpage)
        {

            try
            {
                SqlCnnct con = new SqlCnnct();
                con.Connection();
                con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO PRODUCTTABLE(NAME,PRICE) VALUES (@Name,@Price)", con.Cnnct);
                SqlCommand cmd2 = new SqlCommand("INSERT INTO BOOKTABLE(ISBN,AUTHOR,PUBLISHER,PAGE,COVERPAGE) VALUES (@Isbn,@Author,@Publisher,@Page,@Coverpage)", con.Cnnct);
                if (txtName.Text == "" || txtPrice.Text == ""|| txtISBN.Text == "" || txtAuthor.Text == "" || txtPage.Text == "" || txtPublisher.Text == "" || txtCoverpage.Text == "")
                {
                    MessageBox.Show("Please fill the blanks!", "WARNING!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Price", txtPrice.Text);
                    cmd2.Parameters.AddWithValue("@Isbn", txtISBN.Text);
                    cmd2.Parameters.AddWithValue("@Author", txtAuthor.Text);
                    cmd2.Parameters.AddWithValue("@Publisher", txtPage.Text);
                    cmd2.Parameters.AddWithValue("@Page", txtPublisher.Text);
                    cmd2.Parameters.AddWithValue("@Coverpage", txtCoverpage.Text);
                    cmd.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    
                    con.Close();
                    MessageBox.Show("Registration is succeed!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }

            catch
            {
                MessageBox.Show("Registration is not succeed!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
           

        }

        
    }
}
