﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class UC_AdminProduct : UserControl
    {
        SqltoObj so = new SqltoObj();
       
        AddProduct ap;
        public UC_AdminProduct()
        {
            InitializeComponent();
            so.mergeTable();
            dgvProducts.DataSource = so.getProductTable();
        }



        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add the New Product ", DateTime.Now.ToString());

            ap = new AddProduct();
            ap.ShowDialog();
            if (ap.IsDisposed)
            {
                dgvProducts.Refresh();
                so.mergeTable();
                this.Refresh();
            }
        }
    }
}
