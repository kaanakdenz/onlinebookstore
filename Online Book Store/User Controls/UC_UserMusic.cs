﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class UC_UserMusic : UserControl
    {
        SqltoObj src = new SqltoObj();
        int control;
        ShoppingCart cart = ShoppingCart.GetInstance();

        public UC_UserMusic()
        {
            InitializeComponent();
            src.pullMusicData();
            dgvMusics.DataSource = src.getmusicSrc();
        }

        private void dgvMagazines_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < src.getMusicList().Count(); i++)
            {
                dgvMusics.Rows[i].Cells["COVER"].Value = Image.FromFile(src.getMusicList()[i].Coverpage);

            }
            dgvMusics.ClearSelection();
            timer1.Stop();
        }

        private void UC_UserMusic_Load(object sender, EventArgs e)
        {
            ((DataGridViewImageColumn)this.dgvMusics.Columns["COVER"]).DefaultCellStyle.NullValue = null;
            dgvMusics.Columns.Remove("COVERPAGE");
            dgvMusics.ClearSelection();
        }

        private void btnAddtoCart_Click(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add into the Shopping Cart", DateTime.Now.ToString());

            foreach (DataGridViewRow row in dgvMusics.Rows)
            {
                if (!Convert.ToBoolean(row.Cells[0].Value))
                {
                    control++;
                }
            }


            if (control != 5)
            {

                foreach (DataGridViewRow row in dgvMusics.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value))
                    {
                       
                        ItemToPurchase item = new ItemToPurchase();
                        item.Product = row.Cells[3].Value.ToString();
                        item.Price = Convert.ToInt32(row.Cells[4].Value);
                       

                        if (Convert.ToInt32(row.Cells[2].Value) != 0)
                        {
                            item.Quantity = Convert.ToInt32(row.Cells[2].Value);
                        }
                        else
                        {
                            item.Quantity = 1;
                        }

                        cart.addProduct(item);
                        cart.PaymentAmount += item.Quantity * item.Price;
                    }

                    
                }
                MessageBox.Show("Items added to cart!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                MessageBox.Show("Please select item for add to cart!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        
        }
    }
}
