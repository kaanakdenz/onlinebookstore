﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Online_Book_Store
{
    public partial class UC_UserMagazine : UserControl
    {
        SqltoObj src = new SqltoObj();
        int control;
        ShoppingCart crt = ShoppingCart.GetInstance();

        public UC_UserMagazine()
        {
            InitializeComponent();
            src.pullMagazineData();
            dgvMagazines.DataSource = src.getMgznSource();                    

        }   
     

        private void timer1_Tick_1(object sender, EventArgs e)
        {

            for (int i = 0; i < src.GetMagazines().Count(); i++)
            {
                dgvMagazines.Rows[i].Cells["COVER"].Value = Image.FromFile(src.GetMagazines()[i].Coverpage);

            }
            dgvMagazines.ClearSelection();
            timer1.Stop();
        }

        private void UC_UserMagazine_Load(object sender, EventArgs e)
        {
            ((DataGridViewImageColumn)this.dgvMagazines.Columns["COVER"]).DefaultCellStyle.NullValue = null;
            dgvMagazines.Columns.Remove("COVERPAGE");
            dgvMagazines.ClearSelection();
        }

        private void btnAddtoCart_Click(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add into the Shopping Cart", DateTime.Now.ToString());

            foreach (DataGridViewRow row in dgvMagazines.Rows)
            {
                if (!Convert.ToBoolean(row.Cells[0].Value))
                {
                    control++;
                }
            }


            if (control != 5)
            {

                foreach (DataGridViewRow row in dgvMagazines.Rows)
                {
                    Log.Logging(LoginScreen.userName, this.Name, "Clicked to edit for the Products", DateTime.Now.ToString());
                    if (Convert.ToBoolean(row.Cells[0].Value))
                    {
                        
                        ItemToPurchase item = new ItemToPurchase();
                        item.Product = row.Cells[3].Value.ToString();
                        item.Price = Convert.ToInt32(row.Cells[4].Value);
                       

                        if (Convert.ToInt32(row.Cells[2].Value) != 0)
                        {
                            item.Quantity = Convert.ToInt32(row.Cells[2].Value);
                        }
                        else
                        {
                            item.Quantity = 1;
                        }

                        crt.addProduct(item);
                        crt.PaymentAmount += item.Quantity * item.Price;
                    }
                }
                MessageBox.Show("Items added to cart!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                MessageBox.Show("Please select item for add to cart!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        
        }
    }
}
