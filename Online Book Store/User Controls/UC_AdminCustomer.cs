﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Book_Store
{
    public partial class UC_AdminCustomer : UserControl
    {
        SqltoObj src = new SqltoObj();
        RegisterScreen rs;
        public UC_AdminCustomer()
        {
            InitializeComponent();
            src.pullData();
            dgvCustomers.DataSource=src.getDTSource();
           


        }

       

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add the New Customer", DateTime.Now.ToString());

            rs = new RegisterScreen();
            rs.lblCreate.Text = "Add Account";
            rs.lblReg.Text = "ADMIN PANEL";
            rs.chkAdmin.Show();
            rs.ShowDialog();
           
          
            if (rs.IsDisposed)
            {
                dgvCustomers.Refresh();
                src.pullData();
                this.Refresh();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private void dgvCustomers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
