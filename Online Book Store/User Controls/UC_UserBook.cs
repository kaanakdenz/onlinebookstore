﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Online_Book_Store
{
    public partial class UC_UserBook : UserControl
    {
        SqltoObj src = new SqltoObj();
        ShoppingCart cart = ShoppingCart.GetInstance();
        int control;

        public UC_UserBook()
        {
            InitializeComponent();         

        }

        private void UC_UserBook_Load(object sender, EventArgs e)
        {
           
            OnlineBookStoreEntities3 db = new OnlineBookStoreEntities3();
            bOOKTABLEBindingSource.DataSource = db.BOOKTABLE.ToList();
            src.pullBookData();
            timer1.Start();
            
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < src.getBookList().Count; i++)
            {
                dgvBooks.Rows[i].Cells[3].Value = src.getBookList()[i].ProductName_;
            }

            for (int i = 0; i < src.getBookSource().Rows.Count; i++)
            {
                dgvBooks.Rows[i].Cells[4].Value = src.getBookList()[i].ProductPrice_;
            }

            timer1.Stop();
            dgvBooks.ClearSelection();
        }

        private void btnAddtoCart_Click(object sender, EventArgs e)
        {
            Log.Logging(LoginScreen.userName, this.Name, "Clicked to add into the Shopping Cart", DateTime.Now.ToString());
            foreach (DataGridViewRow row in dgvBooks.Rows)
            {
                if (!Convert.ToBoolean(row.Cells[0].Value))
                {
                    control++;
                }
            }


            if (control != 5)
            {

                foreach (DataGridViewRow row in dgvBooks.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value))
                    {
                        
                        ItemToPurchase item = new ItemToPurchase();
                        item.Product = row.Cells[3].Value.ToString();
                        item.Price = Convert.ToInt32(row.Cells[4].Value);
                        
                        if (Convert.ToInt32(row.Cells[2].Value) != 0)
                        {
                            item.Quantity = Convert.ToInt32(row.Cells[2].Value);
                        }
                        else
                        {
                            item.Quantity = 1;
                        }
                       
                        cart.addProduct(item);
                        cart.PaymentAmount += item.Quantity * item.Price;
                    }
                }
                MessageBox.Show("Items added to cart!", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }

            else
            {
                MessageBox.Show("Please select item for add to cart!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
